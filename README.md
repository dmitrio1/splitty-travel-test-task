
## What has been done so far 
### Search page 
1. Page is responsive and should support mobile & desktop, as displayed. -- check
2. Input:
○ Destination - autocomplete (fixed list based on json which can be found in appendix). -- check
○ Check-in/out - date pickers -- check
○ Adults/children - numeric textbox -- check
3. Apply form validation. -- check
4. Click on search should simulate an ajax request, which returns the hotels list json response, which can be found in appendix. -- check
5. Search criteria should be saved and restored on future visits of users. -- check

### Results page 
1. Page is responsive and should support mobile & desktop, as displayed. -- check
2. Filter box:
○ Hotel name - should display hotels which contain the string. -- check
○ Hotel rating (​bonus​) - should display hotels which have similar rating (multi). -- check
3. List should implement infinite loading approach (10 hotels at a time). -- check
4. Only hotels’ images in viewport should be displayed. -- please, check my implementation of virtual list https://bitbucket.org/dmitrio1/test_task_3/src/master/ -- it renders only nodes that are within viewport. in this example i managed to render 1000000 items in single list.  
5. Filter box in mobile devices should be displayed in a side-bar. -- check
6. If no hotels are found in the selected destination, a message should be displayed. -- check

### Bonus Tasks

1. ● Add unit & integration test. -- search form tests done.

### In progress 

1. Proper styles 