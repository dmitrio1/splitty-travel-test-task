import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

import styles from './header.module.scss';

const Header = props => {
    return (
        <header className={styles.root}>
            <h1>
                <Link to='/' className={styles.logoBlock}>
                        <img className={styles.logoImg} src="https://place-hold.it/100x100/" alt="logo"/>
                        <span className={styles.logoText}>travolta</span>
                </Link>
            </h1>
            <nav>
                <ul className={styles.navList}>
                    <li className={styles.navListItem}>
                        <Link className={styles.navListLink} to='/about-us'>About Us</Link>
                    </li>
                    <li className={styles.navListItem}>
                        <Link className={styles.navListLink} to='/profile/booking'>My Bookings</Link>
                    </li>
                    <li className={styles.navListItem}>
                        <Link className={styles.navListLink} to='/profile/sign-in'>Sign-In</Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}

Header.propTypes = {

}

export default Header
