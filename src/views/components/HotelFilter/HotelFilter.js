import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './hotel-filter.module.scss';

import HotelRating from '../../components/HotelRating';

/**
 * @TODO make utils, move this and all helper fns to utils
 */
const toggleArrEl = (array, value) => {
    var index = array.indexOf(value);
    let res = [];
   
    if (index === -1) {
       res  = array.concat(value);
    } else {
        res = array.slice(0, index).concat(array.slice(index + 1))
    }

    return res;
}

const HotelFilter = props => {
    const { onFilter } = props;
    const allowedRatings = [5, 4, 3, 2, 1];

    const [name, setName] = useState('');
    const [ratings, setRatings] = useState(allowedRatings);

    const toggleRatings = (val) => {
        setRatings(toggleArrEl(ratings, val))
    }
    
   

    return (
        <div className={styles.root}>
            <form action="" onSubmit={(e) => {
                e.preventDefault();
                onFilter({
                ratings,
                name
            })
            }}>
                <fieldset>
                    <legend>Filter by name</legend>
                    <input type="text"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                </fieldset>
                <fieldset>
                    <legend>Filter by rating</legend>
                    {
                        allowedRatings.map(r => <div key={r.key} className={styles.filterRatingItem}>
                            <input type='checkbox' checked={ratings.includes(r)} onChange={() => {toggleRatings(r)}} />
                            <HotelRating ratingNum={r} />
                        </div>
                        )
                    }
                </fieldset>
                <button>
                    Apply filter
                </button>
                <button type='button' onClick={() => 
                    onFilter({
                        name: '',
                        ratings: allowedRatings
                    })
                }>
                    Reset Filter
                </button>
            </form>
        </div>
    )
}

HotelFilter.propTypes = {
    onFilter: PropTypes.func.isRequired,
}

export default HotelFilter;
