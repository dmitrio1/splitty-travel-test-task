import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './hotel-card.module.scss';
import HotelRating from '../../components/HotelRating';

const HotelCard = props => {
    const { hotelItem } = props;
    return (
        <div className={styles.root}>
            <img src="https://place-hold.it/120" alt={`${hotelItem.name} photo`} />
            <div className={styles.hotelInfo}>
                <span>{hotelItem.name}</span>
                <span><HotelRating ratingNum={hotelItem.hotel_rating} /></span>
                <span>{hotelItem.address}</span>
            </div>
            <Link className={styles.viewDetails}><button>View Details</button></Link>
        </div>
    )
}

HotelCard.propTypes = {
    hotelItem: PropTypes.shape({
        //todo define hotel shape 
    }),
}

export default HotelCard
