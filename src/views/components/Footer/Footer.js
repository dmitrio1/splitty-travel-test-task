import React from 'react'
import PropTypes from 'prop-types'

import styles from './footer.module.scss';


const Footer = props => {
    return (
        <footer className={styles.root}>
            Travel with <em>Travolta</em> 
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur mi lacus, non interdum arcu vestibulum quis. Ut condimentum auctor dolor vitae blandit. Duis tincidunt ac felis in convallis. Nullam leo ex, ultricies non congue ac, vestibulum non felis. Fusce eget viverra tortor. Fusce efficitur sem et pulvinar tempor. Vivamus sed est hendrerit, varius velit eu, consectetur purus. Quisque sed nibh pharetra, sodales risus in, finibus tortor. Ut sed diam metus. Aliquam blandit non mi vitae porttitor. Pellentesque vel elit libero. Aenean tempus mauris ut mi vestibulum, eget pulvinar augue viverra. Morbi consectetur, nibh a eleifend tincidunt, arcu felis sagittis purus, tincidunt fermentum turpis ligula ut augue. Nullam tincidunt, felis molestie rhoncus sollicitudin, mauris tellus aliquam lectus, at tempor velit ante vel sapien. Proin mattis elit elit, id ullamcorper purus elementum a. Duis sit amet elit venenatis, congue ex consectetur, dapibus eros.</p>
        </footer>
    )
}

Footer.propTypes = {

}

export default Footer;
