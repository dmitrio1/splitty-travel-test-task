import React from 'react'
import PropTypes from 'prop-types'

import cn from 'classnames';
import styles from './hotel-rating.module.scss';

const HotelRating = props => {
    const { ratingNum, ratingMax = 5 } = props;
    
    const nTimes = new Array(ratingMax);

    return (
        <div className={styles.root}>
                {
                [...nTimes].map((e, i) => {
                    const cls = cn(styles.starIcon, {
                        [styles.disabled]: i >= ratingNum
                    })

                    return <i className={cls} />
                })}
            
        </div>
    )
}

HotelRating.propTypes = {
    ratingNum: PropTypes.number.isRequired,
    ratingMax: PropTypes.number,
}

export default HotelRating
