
import React from 'react';
import { mount } from 'enzyme';
import { withRouter, makeMountRender } from '../../../utils/testUtils';
import {
    BrowserRouter as Router
} from "react-router-dom";
import HotelSearchForm from './HotelSearchForm';
import { time } from '../../../enums';
import styles from './hotel-search-from.module.scss';

/**
 * @TODO add test utils
 */

const initialState = {
    adults: 2,
    children: 0,
    checkIn: new Date().toISOString().substr(0, 10),
    checkOut: new Date(new Date().getTime() + time.DAY * 2).toISOString().substr(0, 10),
    destination: 'Boston'
}

const invalidState = {
    adults: 0,
    children: 0,
    checkIn: new Date().toISOString().substr(0, 10),
    checkOut: new Date(new Date().getTime() - time.DAY * 2).toISOString().substr(0, 10),
    destination: ''
}

describe('Main search form tests', () => {
    it('Should runs submit if form data is valid', () => {
        const handleSearch = jest.fn();
        const wrapper = mount(<Router>
            <HotelSearchForm
                handleSearch={handleSearch}
                initialState={initialState}
            />
        </Router>)
        const form = wrapper.find('form');
        form.simulate('submit');
        expect(handleSearch).toHaveBeenCalledTimes(1);
    });


    it('Should display errors if form data is invalid', () => {
        const handleSearch = jest.fn();
        const wrapper = mount(<Router>
            <HotelSearchForm
                handleSearch={handleSearch}
                initialState={invalidState}
            />
        </Router>)
        const form = wrapper.find('form');
        form.simulate('submit');
      
        expect(wrapper.find(`.${styles.error}`)).toHaveLength(3);

    });

    it('Should call submit function with correct form data', () => {
        const handleSearch = jest.fn();
        const wrapper = mount(<Router>
            <HotelSearchForm
                handleSearch={handleSearch}
                initialState={invalidState}
            />
        </Router>)
        const inputs = wrapper.find('input');

        const dest = inputs.at(0);
        const ci = inputs.at(1);
        const co = inputs.at(2);
        const ad = inputs.at(3);
        const ch = inputs.at(4);
        
        dest.simulate('change', { target: { value: 'Boston' } });
        ci.simulate('change', { target: { value: '2020-02-13' }});
        co.simulate('change', { target: { value: '2020-02-23' }});
        ad.simulate('change', { target: { value: 5 }});
        ch.simulate('change', { target: { value: 25 }});

        const form = wrapper.find('form');

        form.simulate('submit');

        expect(handleSearch.mock.calls[0][0]).toEqual({
            destination: 'Boston',
            checkIn: '2020-02-13',
            checkOut: '2020-02-23',
            adults: 5,
            children: 25
        })

    });

    /**
     * @TODO more tests
     */


})