import React, { useState } from 'react'
import debounce from '../../../utils/debounce';
import { setNumeric } from '../../../utils/setters';

import styles from './hotel-search-form.module.scss';

import {
  MIN_CHAR_BEFORE_SEARCH,
  DELAY_BEFORE_SEARCH,
} from './constants';



import { useHistory, withRouter } from 'react-router-dom';

const HotelSearchForm = props => {
  const {
    handleSearch,
    getAutocompleteList,
    initialState
  } = props;

  const [formData, setFormData] = useState(initialState);

  const [formErrors, setFormErrors] = useState({});
  /**
   * @TOOD  display validation errors
   */

  const [autoCompleteList, setAutocompleteList] = useState([]);

  const history = useHistory();

  const setFormDataField = field => val => setFormData({ ...formData, [field]: val });

  const setDestination = (val) => {
    const setDestinationField = setFormDataField('destination');
    setDestinationField(val);
  }

  const setCheckInDate = (e) => {
    const date = new Date(e.target.value);
    const setCheckIn = setFormDataField('checkIn');
    if (Object.prototype.toString.call(date) === "[object Date]") {
      setCheckIn(date.toISOString().substr(0, 10));
    }
  }

  const setCheckOutDate = (e) => {
    const date = new Date(e.target.value);
    const setCheckOut = setFormDataField('checkOut');

    if (new Date(date).getTime() > new Date(formData.checkIn).getTime()) {
      setCheckOut(date.toISOString().substr(0, 10));
    }
  }

  const setAdults = e => setNumeric(setFormDataField('adults'))(e.target.value);
  const setChildren = e => setNumeric(setFormDataField('children'))(e.target.value);

  const debouncedGetAutocompleteList = debounce((query) => {
    if (query.length > MIN_CHAR_BEFORE_SEARCH) {
      getAutocompleteList(query)
        .then((res) => {
          setAutocompleteList(res);
        });
    }
  }, DELAY_BEFORE_SEARCH);

  const validateForm = (formData) => {

    const validationErrors = {};


    if (new Date(formData.checkOut).getTime() <= new Date(formData.checkIn).getTime()) {
      validationErrors.checkOut = {
        message: 'Invalid time range'
      }
    }

    if (!formData.adults && !formData.children) {
      validationErrors.adults = {
        message: 'Invalid person number'
      }
    }

    if (!formData.destination) {
      validationErrors.destination = {
        message: 'Invalid destination'
      }
    }

    return validationErrors;

  };

  const storeSearchCriteria = (obj) => {
    let criteria = localStorage.getItem('searchCriteria') || '[]';

    let criteriaArr;

    try {
      criteriaArr = JSON.parse(criteria);
    }
    catch (e) {
      console.log('Unable to parse user search history');
    }

    if (!Array.isArray(criteriaArr)) {
      criteriaArr = [];
    }

    criteriaArr.push(obj);

    localStorage.setItem('searchCriteria', JSON.stringify(criteriaArr));

  }


  /**
   * @TODO split autocomplete, make  reusable component and write tests for it
   */
  return (
    <form action="" onSubmit={(e) => {
      e.preventDefault();
      const errors = validateForm(formData);

      setFormErrors(errors);

      if (Object.values(errors).length > 0) {
        return;
      }

      storeSearchCriteria(formData);
      // i store search history, but where i should display it?

      handleSearch(formData);
      history.push('/hotels');
    }}>
      <div className={styles.fieldset}>
        <div className={styles.field}>
          <label htmlFor="destination">
            Destination
              </label>
          <datalist id='autocompleteList'>
            {
              autoCompleteList.map(item => <option key={item.id}>{item.name}</option>)
            }
          </datalist>
          <input
            name='destination'
            id='destination'
            value={formData.destination}
            autoComplete='off'
            list='autocompleteList'
            onChange={(e) => {
              let val = e.target.value;
              debouncedGetAutocompleteList(val);
              setDestination(val);
            }}
          />
          {formErrors.destination &&  <p className={styles.error}>{formErrors.destination.message}</p>}
        </div>
        <div className={styles.field}>
          <label htmlFor="check-in">
            Check-in
              </label>
          <input
            name='check-in'
            id='check-in'
            type='date'
            value={formData.checkIn}
            onChange={setCheckInDate}
          />
          { formErrors.checkIn && <p className={styles.error}>{ formErrors.checkIn.message}</p>}
        </div>
        <div className={styles.field}>
          <label htmlFor="check-out">
            Check-out
          </label>
          <input
            type='date'
            id='check-out'
            name='check-out'
            value={formData.checkOut}
            onChange={setCheckOutDate}
          />
          {formErrors.checkOut && <p className={styles.error}>{formErrors.checkOut.message}</p>}
        </div>
        <div className={styles.row}>
          <div className={styles.field}>
            <label htmlFor="adults">
              Adults
            </label>
            <input
              name='adults'
              id='adults'
              value={formData.adults}
              onChange={setAdults}
            />
            {formErrors.adults && <p className={styles.error}>{formErrors.adults.message}</p>}
          </div>
          <div className={styles.field}>
            <label htmlFor="children">
              Children
                  </label>
            <input
              name='children'
              id='children'
              value={formData.children}
              onChange={setChildren}
            />
            {formErrors.children && <p className={styles.error}>{formErrors.children.message}</p>}
          </div>
        </div>
        <button className={styles.searchBtn}>Search</button>
      </div>
    </form>
  )
}

HotelSearchForm.propTypes = {

}

export default HotelSearchForm
