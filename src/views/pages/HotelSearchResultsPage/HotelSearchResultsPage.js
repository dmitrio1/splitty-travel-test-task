import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames';

import HotelCard from '../../components/HotelCard';
import HotelFilter from '../../components/HotelFilter';

import getHotels from '../../../FakeAPI/hotels';



import styles from './hotel-search-results-page.module.scss';



const HotelSearchResultsPage = props => {
    const { searchParams } = props;

    const [isSidebarVisible, setSidebarVisible] = useState(false);
    // const [currentPage, setCurrentPage] = useState(0);

    const [filter, setFilter] = useState({
        name: '',
        ratings: [1, 2, 3, 4, 5]
    });
    const [hotels, setHotels] = useState({
        list: [],
        totalLength: 0,
        currentPage: 0
    });

    console.log(hotels, 'hotels')

    const loadNext = () => {
        const params = {
            ...searchParams,
            ...filter
        };

        if (Object.values(params).length > 0) {
            getHotels(params, hotels.currentPage + 1)
                .then(res => {
                    setHotels({
                        ...res,
                        list: [...hotels.list, ...res.list],
                        currentPage: hotels.currentPage + 1
                    })
                });
        }
    }

    useEffect(() => {
        const startPageNum = 0;
        
        if (ref.current) {
            ref.current.scrollTop = 0;
        }
        const params = {
            ...searchParams,
            ...filter
        };

        if (Object.values(params).length > 0) {
            getHotels(params, startPageNum)
                .then(res => {
                    setHotels({...hotels, ...res, currentPage: startPageNum});
                });
        }

    }, [searchParams, filter])

    const ref = useRef(null);

    // todo make custom hook;
    const handleScroll = (event) => {

        var element = event.target;
        // console.log(element.scrollHeight - element.scrollTop, element.clientHeight)
        if (element.scrollHeight - element.scrollTop <= element.clientHeight) {
            
            if (hotels.list.length < hotels.totalLength) {
                loadNext();
            }
        }
    };

    useEffect(() => {
        const element = ref.current;

        if (element) {
            element.addEventListener('scroll', handleScroll);
        }

        return () => {
            element.removeEventListener('scroll', handleScroll);
        };
    })


    // const filteredHotelList =  hotels;
    /**
     *  infinite scroll + lazy loading can be done with some out of the box  solution
     */

    const sidebarClsName = cn(styles.sidebar, {
        [styles.animateSb]: isSidebarVisible
    })

    return (
        <div className={styles.root}>
            <button className={styles.toggleFilterBtn} onClick={() => setSidebarVisible(!isSidebarVisible)}>
                Toggle Filter
            </button>
            <div className={styles.container}>
                <aside className={sidebarClsName}>
                    <HotelFilter onFilter={(filterObj) => { setFilter(filterObj) }} />
                </aside>
                <ul ref={ref} className={styles.list}>

                    {
                        hotels.list.length ? hotels.list.map(h => <HotelCard key={h.id} hotelItem={h} />)
                            : 'Nothing found'
                    }
                </ul>
            </div>
        </div>
    )
}

HotelSearchResultsPage.propTypes = {
    hotels: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        //todo define hotel shape 
    })),
}

export default HotelSearchResultsPage
