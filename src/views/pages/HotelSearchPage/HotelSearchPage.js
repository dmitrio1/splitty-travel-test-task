import React from 'react';
import PropTypes from 'prop-types';
import styles from './hotel-search-page.module.scss';


import {
  time
} from '../../../enums';

import HotelSearchForm from '../../components/HotelSearchForm';
/**
 * @TODO split this large component 
 */

const HotelSearchPage = props => {

  const {
    handleSearch,
    getAutocompleteList
  } = props;
  //todo split form to component
  const initialState = {
    adults: 2,
    children: 0,
    checkIn: new Date().toISOString().substr(0, 10),
    checkOut: new Date(new Date().getTime() + time.DAY * 2).toISOString().substr(0, 10),
    destination: ''
  }
  return (
    <div className={styles.root}>
      <HotelSearchForm 
        handleSearch={handleSearch} 
        getAutocompleteList={getAutocompleteList}
        initialState={initialState}
        />
    </div >
  )
}

HotelSearchPage.propTypes = {
  handleSearch: PropTypes.func.isRequired,
  getAutocompleteList: PropTypes.func.isRequired,
}

export default HotelSearchPage
