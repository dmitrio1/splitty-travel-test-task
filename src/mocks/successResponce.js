const successResponce = [
    {
      "id": 5018105,
      "name": "Woogo Central Park - Tempo Apartments",
      "address": "240 West 73rd Street",
      "city": "New York",
      "state": null,
      "country_code": "US",
      "hotel_rating": 4,
      "phone_number": "18005740835",
      "website": null
    },
    {
      "id": 11003176,
      "name": "Amolite Hotel",
      "address": "Avenida Curitiba, 811",
      "city": "New York",
      "state": null,
      "country_code": "BR",
      "hotel_rating": 0,
      "phone_number": null,
      "website": null
    },
    {
      "id": 747134,
      "name": "Redford Hotel",
      "address": "136 Ludlow Street",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 3.5,
      "phone_number": "212260414",
      "website": null
    },
    {
      "id": 720550,
      "name": "Hotel Richland New York",
      "address": "5 Allen Street",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 3,
      "phone_number": "1 212 2191018",
      "website": null
    },
    {
      "id": 797074,
      "name": "Studio Lux Times Square",
      "address": "between 9 Avenue and 8 Avenue",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 2.5,
      "phone_number": null,
      "website": null
    },
    {
      "id": 742987,
      "name": "The Bowery Hotel",
      "address": "335 Bowery",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 4,
      "phone_number": null,
      "website": null
    },
    {
      "id": 743452,
      "name": "AC Hotel by Marriott New York Times Square",
      "address": "260 West 40th Street",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 3.5,
      "phone_number": "1-212-398-2700",
      "website": null
    },
    {
      "id": 260469,
      "name": "The Ridge Hotel",
      "address": "151 East Houston Street",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 3,
      "phone_number": "1 212 777-0012",
      "website": null
    },
    {
      "id": 740399,
      "name": "SoHo 54 Hotel",
      "address": "54 Watts St",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 3,
      "phone_number": null,
      "website": null
    },
    {
      "id": 5018104,
      "name": "Hotel Alexander",
      "address": "306 W 94th St",
      "city": "New York",
      "state": null,
      "country_code": "US",
      "hotel_rating": 3,
      "phone_number": "XX",
      "website": null
    },
    {
      "id": 792811,
      "name": "Parker New York, A Hyatt Affiliate Property",
      "address": "119 West 56th Street",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 4.5,
      "phone_number": null,
      "website": null
    },
    {
      "id": 804220,
      "name": "Central Park View B&B",
      "address": "301 West 110th Street",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 2.5,
      "phone_number": null,
      "website": null
    },
    {
      "id": 5018107,
      "name": "Staten Island Hotel",
      "address": "1415 Richmond Ave",
      "city": "New York",
      "state": null,
      "country_code": "US",
      "hotel_rating": 3,
      "phone_number": "17186985000",
      "website": null
    },
    {
      "id": 700390,
      "name": "Mondrian Park Avenue",
      "address": "444 Park Ave S",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 4.5,
      "phone_number": null,
      "website": null
    },
    {
      "id": 742946,
      "name": "Pod Times Square",
      "address": "400 W 42nd Street",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 3,
      "phone_number": null,
      "website": null
    },
    {
      "id": 776932,
      "name": "Freehand New York",
      "address": "23 Lexington Ave",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 4,
      "phone_number": "121-247-51920",
      "website": "https://freehandhotels.com/new-york/rooms/"
    },
    {
      "id": 741741,
      "name": "The Grand NYC",
      "address": "38 West 31st Street",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 2,
      "phone_number": null,
      "website": null
    },
    {
      "id": 716815,
      "name": "Hello Broadway",
      "address": "38 west 31 st",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 3,
      "phone_number": null,
      "website": null
    },
    {
      "id": 802583,
      "name": "The Dominick",
      "address": "246 Spring Street",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 5,
      "phone_number": "+1 212 842 5500",
      "website": null
    },
    {
      "id": 5018106,
      "name": "HomeStay NYC",
      "address": "49 Catherine Street",
      "city": "New York",
      "state": null,
      "country_code": "US",
      "hotel_rating": 3,
      "phone_number": null,
      "website": null
    },
    {
      "id": 751736,
      "name": "Fairfield Inn & Suites by Marriott/World Trade Center Area",
      "address": "100 Greenwich St",
      "city": "New York",
      "state": "NY",
      "country_code": "US",
      "hotel_rating": 3,
      "phone_number": "703-261-5677",
      "website": null
    }
  ];


  export default successResponce;