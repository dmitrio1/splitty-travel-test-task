import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import getHotels from './FakeAPI/hotels';
import getAutocompleteList from './FakeAPI/destinations';

import HotelSearchPage from './views/pages/HotelSearchPage';
import HotelSearchResultsPage from './views/pages/HotelSearchResultsPage';
import Header from './views/components/Header';
import Footer from './views/components/Footer';

import './App.css';

function App() {

  // const [hotels, setHotels] = useState([]);
  const [params, setParams] =  useState({});

  return (
    <Router>
      <div className="App">
        <Header />
        <main>
          <Switch>
            <Route exact path='/'>
              <HotelSearchPage getAutocompleteList={getAutocompleteList} handleSearch={setParams} />
            </Route>
            <Route  path='/hotels' >
              <HotelSearchResultsPage searchParams={params} />
            </Route>
          </Switch>
        </main>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
