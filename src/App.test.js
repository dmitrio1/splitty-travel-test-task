import React from 'react';
import { mount, shallow } from 'enzyme';

import {
  BrowserRouter as Router
} from "react-router-dom";
import App from './App';
import successResponce from './mocks/successResponce';
import { act } from 'react-dom/test-utils';

describe('Main App tests', () => {

  it('renders without crashing', () => {
    shallow(<App />);
  });


  it('Redirects user to hotels page', () => {
    const wrapper = mount(<Router>
      <App />
    </Router>)
    
      const inputs = wrapper.find('input');
      const dest = inputs.at(0);
      dest.simulate('change', { target: { value: 'Boston' } });

      const form = wrapper.find('form');
      form.simulate('submit');


    expect(global.location.pathname).toBe('/hotels')

  })


})
