import React from 'react';
import { mount } from 'enzyme';
import {
    BrowserRouter as Router
} from "react-router-dom";

export const withRouter = (Component, props = {}, state = {}) => {
        return (props) => (<Router>
                <Component {...props} />
            </Router>
        );
};

export const makeMountRender = (Component, defaultProps = {}) => {

    return (customProps = {}) => {
      const props = {
        ...defaultProps,
        ...customProps
      };

      return mount(<Component {...props} />);
    };
  };