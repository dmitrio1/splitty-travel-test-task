const debounce  = (f, t=1000) => {
    let timer;
    return function (args) {
      if(timer){
        clearTimeout(timer);
      }
  
      timer = setTimeout(() => f(args), t)
    }
  }

export default debounce; 