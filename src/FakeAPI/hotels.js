import hotels from '../mocks/hotels';

const PAGE_SIZE = 10;

const getHotels = (params, page=0) => {

    return new Promise((resolve, reject) => {
        // console.log(params, page, 'params');
        const startIndex = page * PAGE_SIZE;
        
        const filteredHotels = hotels
        .filter(hotel => {
            if(params.destination && hotel.city.toLowerCase() !== params.destination.toLowerCase()){
                return false
            }
            if(params.name && !hotel.name.toLowerCase().includes(params.name.toLowerCase())){
                return false
            }
            if(params.ratings && !params.ratings.includes(Math.ceil(hotel.hotel_rating))){
                return false
            }
            return true
        });

        const totalLength = filteredHotels.length;

        const currentPage = filteredHotels.slice(startIndex, startIndex + PAGE_SIZE );

        resolve(
          {
              list: currentPage,
              totalLength,
          }
        )
    }
    )
}

export default getHotels;